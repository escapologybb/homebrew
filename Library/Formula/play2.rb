require 'formula'

class Play2 < Formula
  homepage 'http://www.playframework.org/'
  url 'http://download.playframework.org/releases/play-2.0.zip'
  md5 '5b429c991be607c8e41203579c6b506e'

  def install
    rm_rf 'python' # we don't need the bundled Python for windows
    rm Dir['*.bat']
    libexec.install Dir['*']
    bin.install_symlink libexec+'play' => 'play2'
  end
end
